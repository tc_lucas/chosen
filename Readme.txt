Readme.txt

a. Platform and language chosen
	iOS, however it had to be done in Objective-C
b. What are the highlights of your logic/code writing style?
	I've used some of the apples MVC as well as with some concepts of MVVM Architecture.
c. What could have been done in a better way?
	A lot of things acctually if it could have done in swift, things such as parsing objects and converting them into models, all calls from REALM are a way better and easier now with Swift new implementations as it is sort of a functional language.
d. Any other notes you judge relevant for the evaluation of your project.
	I've done an app with a reduced scope, as most of the magic that would have been done in swift was somehow held when developing in Objective-c. I've showed most of the way I work when organising code, good practices, architecture patterns and reusability on this project(even with this smaller scope). Hope you guys stil evaluate it.