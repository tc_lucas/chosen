//
//  AppDelegate.h
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 13/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

