//
//  Constants.h
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 16/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

NSString * const restaurantCellIdentifier = @"restaurant_cell_identifier";

#endif /* Constants_h */

