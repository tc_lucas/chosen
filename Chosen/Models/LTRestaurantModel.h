 //
//  LTRestaurantModel.h
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 16/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTRate.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "INTULocationManager.h"
#import "Realm.h"

@interface LTRestaurantModel : RLMObject

@property NSString *name;
@property double latitude;
@property double longitude;
@property NSString *url;
@property NSString *phoneNumber;
@property NSString *distanceFromUser;
@property int identifier;
@property NSString *typeOfFood;
@property int votes;
@property LTRate *rating;


- (id)initWith:(NSString *)name latitude:(double)latitude longitude:(double)longitude NSURL:(NSString *)url phoneNumber:(NSString *)phoneNumber typeOfFood:(NSString *)typeOfFood votes:(int)votes rates:(LTRate *)rates;
    
@end
RLM_ARRAY_TYPE(LTRestaurantModel)
