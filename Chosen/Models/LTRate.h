//
//  LTRate.h
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 16/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Realm.h"

@interface LTRate : RLMObject

@property double stars;
@property NSString *voteName;

@end
