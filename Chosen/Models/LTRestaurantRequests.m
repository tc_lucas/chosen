//
//  LTRestaurantRequests.m
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 17/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import "LTRestaurantRequests.h"
@interface LTRestaurantRequests()

@end

@implementation LTRestaurantRequests
INTULocationManager *manager;

- (void)getUserLocationWithCompletionBlock:(void (^)(CLLocation *location , INTULocationStatus status))handler{
    manager = [[INTULocationManager alloc] init];
    [manager requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
                                         timeout:10.0
                                           block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                               handler(currentLocation,status);
                                           }];
}

- (void)downloadRestaurantsWithCompletionBlockOfRestaurants:(void (^) (NSMutableArray<LTRestaurantModel *> *response,NSError *error))callBack{

    NSMutableArray<LTRestaurantModel *> *restaurants = (NSMutableArray<LTRestaurantModel *>*)[LTRestaurantModel allObjects];
    if (restaurants.count > 0){
        callBack(restaurants,nil);
        return;
    }
    
    [self getUserLocationWithCompletionBlock:^(CLLocation *location , INTULocationStatus status){
        if (status == INTULocationStatusSuccess) {
            MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
            request.region = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000);
            request.naturalLanguageQuery = @"restaurants"; // or business name
            MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:request];
            [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
                NSMutableArray<LTRestaurantModel *> *call = [self parseObjectsWithDataFromArray:response.mapItems];
                for (LTRestaurantModel *place in call){
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    [realm beginWriteTransaction];

                    [place setDistanceFromUser:[NSString stringWithFormat:@"%.2f Kms Away",[location distanceFromLocation:[[CLLocation alloc] initWithLatitude:place.latitude
                                                                                                                                                     longitude:place.longitude]]/1000]];
                    [realm commitWriteTransaction]; 
                }
                callBack(call,error);
            }];
        }
        else if (status == INTULocationStatusTimedOut) {
            //Handle timeout error
            
        }
        else {
            //Handle generic error
        }
        
        NSLog(@"%@",location);
    }];
}

- (NSMutableArray<LTRestaurantModel *>*)parseObjectsWithDataFromArray:(NSArray *)data{
    //I would have done it with alamofire + alamofireObjectMapper + ObjectMapper frameworks, giving it a much cleaner and faster setup developed in swift*
    NSMutableArray * response = [[NSMutableArray alloc] init];
    RLMRealm *realm = [RLMRealm defaultRealm];
    for (MKMapItem *place in data){
        LTRestaurantModel *rest = [[LTRestaurantModel alloc] initWith:place.name
                                                            latitude:place.placemark.coordinate.latitude
                                                            longitude:place.placemark.coordinate.longitude
                                                                NSURL:[NSString stringWithFormat:@"%@",place.url]
                                                          phoneNumber:place.phoneNumber
                                                           typeOfFood:@"default"
                                                                votes:0
                                                                rates:nil];
        [realm transactionWithBlock:^{
            [realm addObject:rest];
        }];
        [response  addObject:rest];
    }
    
    return response;
}

@end
