//
//  LTRestaurantModel.m
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 16/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import "LTRestaurantModel.h"

@implementation LTRestaurantModel

- (int)identifier{
    return [self.phoneNumber doubleValue];
}

- (id)initWith:(NSString *)name latitude:(double)latitude longitude:(double)longitude NSURL:(NSString *)url phoneNumber:(NSString *)phoneNumber typeOfFood:(NSString *)typeOfFood votes:(int)votes rates:(LTRate *)rates{

    self = [super init];
    if (self){
        [self setIdentifier:arc4random()];
        [self setName:name];
        [self setLatitude:latitude];
        [self setLongitude:longitude];
        [self setUrl:url];
        [self setPhoneNumber:phoneNumber];
        [self setTypeOfFood:typeOfFood];
        [self setVotes:votes];
        [self setRating:rates];
    }
    return self;
}


@end
