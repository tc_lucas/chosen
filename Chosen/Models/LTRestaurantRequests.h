//
//  LTRestaurantRequests.h
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 17/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import "LTRestaurantModel.h"
#import "LTRate.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "INTULocationManager.h"

@interface LTRestaurantRequests : LTRestaurantModel

- (void)downloadRestaurantsWithCompletionBlockOfRestaurants:(void (^) (NSMutableArray<LTRestaurantModel *> *response,NSError *error))callBack;

@end
