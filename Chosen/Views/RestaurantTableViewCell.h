//
//  RestaurantCellTableViewCell.h
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 16/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTRestaurantModel.h"
#import "LTRestaurantRequests.h"

@interface RestaurantTableViewCell : UITableViewCell

@property (weak, nonatomic) LTRestaurantModel * cellObject;
@property bool isVoted;

@property (strong, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *restaurantTypeOfFoodLabel;
@property (strong, nonatomic) IBOutlet UILabel *restaurantDistanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *numberOfVotesLabel;


- (void)reloadCell;

@end
