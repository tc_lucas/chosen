//
//  RestaurantCellTableViewCell.m
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 16/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import "RestaurantTableViewCell.h"

@implementation RestaurantTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)reloadCell{
    if (_cellObject != nil){
        [self setupCellsLayout];
    }
}

- (void)setupCellsLayout{
    self.restaurantNameLabel.text = @"";
    self.restaurantTypeOfFoodLabel.text = @"";
    self.restaurantDistanceLabel.text = @"";
    self.numberOfVotesLabel.text = @"";
    
    if (self.cellObject != nil){
        self.restaurantNameLabel.text = self.cellObject.name;
        self.restaurantTypeOfFoodLabel.text = @"Didn't find type of food information";
        self.restaurantDistanceLabel.text = self.cellObject.distanceFromUser;
        self.numberOfVotesLabel.text = [NSString stringWithFormat:@"%i",self.cellObject.votes];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
