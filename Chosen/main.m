//
//  main.m
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 13/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
