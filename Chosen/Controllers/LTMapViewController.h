//
//  LTMapViewController.h
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 17/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "LTRestaurantModel.h"
#import "Realm.h"

@interface LTMapViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
@end
