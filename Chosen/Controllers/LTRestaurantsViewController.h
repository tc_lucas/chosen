//
//  LTRestaurantsViewController.h
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 16/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "LTRestaurantRequests.h"
#import "RestaurantTableViewCell.h"

@interface LTRestaurantsViewController : UIViewController<CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource>

@property (retain,nonatomic) CLLocationManager * manager;
@property (retain,nonatomic) LTRestaurantRequests *networkManager;
@property (retain,nonatomic) NSMutableArray<LTRestaurantModel *> *tableViewDataSource;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
