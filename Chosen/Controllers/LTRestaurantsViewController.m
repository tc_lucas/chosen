//
//  LTRestaurantsViewController.m
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 16/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import "LTRestaurantsViewController.h"

@interface LTRestaurantsViewController ()

@end

@implementation LTRestaurantsViewController
#pragma mark - View Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}


#pragma mark - View Controller General Methods

- (void)setupView{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self downloadData];
}

- (void)downloadData{
    self.networkManager = [[LTRestaurantRequests alloc] init];
    
    [self.networkManager downloadRestaurantsWithCompletionBlockOfRestaurants:^(NSArray<LTRestaurantModel *> *results,
                                                                               NSError *error){
        if (results){
            self.tableViewDataSource = results;
            [self.tableView reloadData];
        }else{
            //Handle error.
        }
        NSLog(@"Result: %@ , Error: %@",results,error);
    }];
}

#pragma mark - tableView Delegates

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:restaurantCellIdentifier];
    cell.cellObject = [self.tableViewDataSource objectAtIndex:indexPath.row];
    [cell reloadCell];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LTRestaurantModel *voted = self.tableViewDataSource[indexPath.row];
    [self decrementAllRestaurantsVotesByOneExcept:voted];
    [self incrementVoteForRestaurant:voted];
    [self.tableView reloadData];
}

- (void)incrementVoteForRestaurant:(LTRestaurantModel *)restaurant{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    restaurant.votes = 1;
    [realm commitWriteTransaction];
}

- (void)decrementAllRestaurantsVotesByOneExcept:(LTRestaurantModel *)restaurant{
    NSMutableArray<LTRestaurantModel *> *restaurants = (NSMutableArray<LTRestaurantModel *> *)[LTRestaurantModel allObjects];
    for (LTRestaurantModel * place in restaurants){
        RLMRealm *realm = [RLMRealm defaultRealm];
        if (place.phoneNumber != restaurant.phoneNumber){
            [realm beginWriteTransaction];
            place.votes = 0;
            [realm commitWriteTransaction];
        }
    }
}

#pragma mark - tableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (!self.tableView){
        return 0;
    }
    return [self.tableViewDataSource count];
}

@end
