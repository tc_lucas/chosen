//
//  LTMapViewController.m
//  Chosen
//
//  Created by Lucas Teixeira Carletti on 17/02/2017.
//  Copyright © 2017 DB. All rights reserved.
//

#import "LTMapViewController.h"

@interface LTMapViewController ()
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSMutableArray<LTRestaurantModel *> *restaurants;
@property bool shouldZoom;
@end

@implementation LTMapViewController

#pragma mark - View Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

- (void)setupView{
    self.mapView.showsUserLocation = true;
    self.mapView.delegate = self;
    self.shouldZoom = true;
    [self loadRestaurants];
}

#pragma mark - View Controller General Methods

- (void)zoomMap{
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance (self.mapView.userLocation.location.coordinate, 2000, 2000);
    [self.mapView setRegion:region animated:true];
}

- (void)changeType{
    if (self.mapView.mapType == MKMapTypeStandard)
    self.mapView.mapType = MKMapTypeSatellite;
    else
    self.mapView.mapType = MKMapTypeStandard;
}

- (void)loadRestaurants{
    self.restaurants = (NSMutableArray<LTRestaurantModel *> *)[LTRestaurantModel allObjects];
    for (LTRestaurantModel * rest in self.restaurants){
        MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
        [pin setCoordinate:CLLocationCoordinate2DMake(rest.latitude,
                                                      rest.longitude)];
        [pin setTitle:rest.name];
        [pin setSubtitle:[NSString stringWithFormat:@"Distance: %@",rest.distanceFromUser]];
        [self.mapView addAnnotation:pin];
    }
}

#pragma mark - Actions

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if (self.shouldZoom && userLocation.location.coordinate.latitude != 0 && userLocation.location.coordinate.longitude != 0){
        [self zoomMap];
        self.shouldZoom = false;
    }
}

#pragma mark - Actions

- (IBAction)zoomAction:(UIButton *)sender {
    [self zoomMap];
}
- (IBAction)changeTypeAction:(UIButton *)sender {
    [self changeType];
}

@end
